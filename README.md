# Infinity Search Chrome Extension

## Installation 

Unfortunately, Google makes it much more difficult to upload extensions than Firefox and
 they make you pay to do so. Luckily for everyone, however, it is very easy to manually install extensions 
 into Chrome. 
 <br>
 
 Here is how to do it: 
 
1. Go to https://gitlab.com/infinitysearch/infinity-search-chrome-extension (this page)
2. Click on the download button on the page and then unzip it on your computer 
3. Store the folder wherever you would like it be
4. Open Chrome and go to chrome://extensions/ (if you are using brave then go to to brave://extensions)
5. In the top right corner of the screen turn the developer mode option on
6. Press the load unpacked button that has just appeared towards the top left area of the page
7. Go to the location of the folder and click select

Your Chrome extension should be working now!

